FROM kpengboy/trisquel:belenos

RUN apt-get -q update && apt-get install -q -y wget tar bzip2 git sed patch build-essential

WORKDIR /tmp/toolchain
RUN wget -q https://bitbucket.org/xiannox/trisquel-mingw/raw/HEAD/toolchain-windows.sh
RUN chmod +x toolchain-windows.sh
RUN ./toolchain-windows.sh

WORKDIR /
RUN rm -rf /tmp/toolchain
